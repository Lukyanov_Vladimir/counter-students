package com.example.counterstudents;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Integer counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.counterView);
        textView.setText(counter.toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        counter = savedInstanceState.getInt("valueCounter");

        TextView textView = (TextView) findViewById(R.id.counterView);
        textView.setText(counter.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("valueCounter", counter);
    }

    public void onClickBtnAddStudents(View view) {
        counter++;
        TextView textView = (TextView) findViewById(R.id.counterView);
        textView.setText(counter.toString());
    }

    public void onClickBtnRetest(View view) {
        counter = 0;
        TextView textView = (TextView) findViewById(R.id.counterView);
        textView.setText(counter.toString());
    }
}

